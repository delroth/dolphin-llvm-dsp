// Copyright (C) 2003 Dolphin Project.

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 2.0.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License 2.0 for more details.

// A copy of the GPL 2.0 should have been included with the program.
// If not, see http://www.gnu.org/licenses/

// Official SVN repository and contact information can be found at
// http://code.google.com/p/dolphin-emu/

#include "../DSPLLVMEmitter.h"
#include "../DSPCore.h"
#include "../DSPHWInterface.h"

static const struct {
	const char* name;
	void* addr;
	int bits;
} regs_descriptors[] = {
	{ "AR0", &g_dsp.r.ar[0], 16 },
	{ "AR1", &g_dsp.r.ar[1], 16 },
	{ "AR2", &g_dsp.r.ar[2], 16 },
	{ "AR3", &g_dsp.r.ar[3], 16 },
	{ "IX0", &g_dsp.r.ix[0], 16 },
	{ "IX1", &g_dsp.r.ix[1], 16 },
	{ "IX2", &g_dsp.r.ix[2], 16 },
	{ "IX3", &g_dsp.r.ix[3], 16 },
	{ "WR0", &g_dsp.r.wr[0], 16 },
	{ "WR1", &g_dsp.r.wr[1], 16 },
	{ "WR2", &g_dsp.r.wr[2], 16 },
	{ "WR3", &g_dsp.r.wr[3], 16 },
	{ "ST0", &g_dsp.r.st[0], 16 },
	{ "ST1", &g_dsp.r.st[1], 16 },
	{ "ST2", &g_dsp.r.st[2], 16 },
	{ "ST3", &g_dsp.r.st[3], 16 },
	{ "ACH0", &g_dsp.r.ac[0].h, 16 },
	{ "ACH1", &g_dsp.r.ac[1].h, 16 },
	{ "CR", &g_dsp.r.cr, 16 },
	{ "SR", &g_dsp.r.sr, 16 },
	{ "PRODL", &g_dsp.r.prod.l, 16 },
	{ "PRODM", &g_dsp.r.prod.m, 16 },
	{ "PRODH", &g_dsp.r.prod.h, 16 },
	{ "PRODM2", &g_dsp.r.prod.m2, 16 },
	{ "AXL0", &g_dsp.r.ax[0].l, 16 },
	{ "AXL1", &g_dsp.r.ax[1].l, 16 },
	{ "AXH0", &g_dsp.r.ax[0].h, 16 },
	{ "AXH1", &g_dsp.r.ax[1].h, 16 },
	{ "ACL0", &g_dsp.r.ac[0].l, 16 },
	{ "ACL1", &g_dsp.r.ac[1].l, 16 },
	{ "ACM0", &g_dsp.r.ac[0].m, 16 },
	{ "ACM1", &g_dsp.r.ac[1].m, 16 },
	{ "AX0", &g_dsp.r.ax[0].val, 32 },
	{ "AX1", &g_dsp.r.ax[1].val, 32 },
	{ "ACC0", &g_dsp.r.ac[0].val, 64 },
	{ "ACC1", &g_dsp.r.ac[1].val, 64 },
	{ "PROD", &g_dsp.r.prod.val, 64 },
};

void DSPLLVMEmitter::InitRegs()
{
	for (u32 i = 0; i < sizeof (regs_descriptors) / sizeof (regs_descriptors[0]); ++i)
	{
		cached_regs[i] = GetCppInt(
			regs_descriptors[i].addr,
			regs_descriptors[i].bits,
			regs_descriptors[i].name
		);
	}
}

llvm::Value* DSPLLVMEmitter::LoadRegister(u8 reg, DSPLLVMEmitter::LoadFlags fl)
{
	if (reg >= DSP_REG_ST0 && reg <= DSP_REG_ST3)
	{
		PanicAlert("Loading stack register, not supported");
		return NULL; // TODO: stack registers
	}

	llvm::Value* regval = builder.CreateLoad(cached_regs[reg], regs_descriptors[reg].name);
	if (fl & SEX_ZERO)
		regval = builder.CreateZExt(regval, llvm::IntegerType::get(ctx, 64));
	else if (fl & SEX_SIGN)
		regval = builder.CreateSExt(regval, llvm::IntegerType::get(ctx, 64));

	return regval;
}

void DSPLLVMEmitter::StoreRegister(u8 reg, llvm::Value* val)
{
	builder.CreateStore(val, cached_regs[reg]);
}

void DSPLLVMEmitter::WriteDMEM(llvm::Value* addr, llvm::Value* val)
{
	llvm::BasicBlock* ifx_write = llvm::BasicBlock::Create(ctx, "ifx_write", current_func);
	llvm::BasicBlock* dram_write = llvm::BasicBlock::Create(ctx, "dram_write", current_func);
	llvm::BasicBlock* write_done = llvm::BasicBlock::Create(ctx, "write_done", current_func);

	addr = builder.CreateTrunc(addr, llvm::IntegerType::get(ctx, 16));
	val = builder.CreateTrunc(val, llvm::IntegerType::get(ctx, 16));

	llvm::Value* cmp = builder.CreateICmpUGE(addr, GenUInt16(0x1000));
	builder.CreateCondBr(cmp, ifx_write, dram_write);

	builder.SetInsertPoint(dram_write);
	llvm::Type* dram_type = llvm::PointerType::getUnqual(
		llvm::IntegerType::get(ctx, 16)
	);
	llvm::Value* dram = builder.CreateLoad(GetCppVar(&g_dsp.dram, dram_type, "dram"), "dram");

	std::vector<llvm::Value*> indices;
	indices.push_back(builder.CreateZExt(addr, llvm::IntegerType::get(ctx, 64)));
	llvm::Value* ptr = builder.CreateGEP(dram, indices);
	builder.CreateStore(val, ptr);
	builder.CreateBr(write_done);

	builder.SetInsertPoint(ifx_write);
	addr = builder.CreateZExt(addr, llvm::IntegerType::get(ctx, 32));
	val = builder.CreateZExt(val, llvm::IntegerType::get(ctx, 32));

	std::vector<llvm::Type*> args_types;
	args_types.push_back(llvm::IntegerType::get(ctx, 32));
	args_types.push_back(llvm::IntegerType::get(ctx, 32));
	llvm::FunctionType* ft = llvm::FunctionType::get(
		llvm::Type::getVoidTy(ctx), args_types, false
	);
	GenFunctionCall((void*)gdsp_ifx_write, ft, "gdsp_ifx_write", 2, addr, val);
	builder.CreateBr(write_done);

	builder.SetInsertPoint(write_done);
}

void DSPLLVMEmitter::IncrementAddrReg(u8 reg, llvm::Value* current_val)
{
	u8 ar_id = reg, wr_id = DSP_REG_WR0 + reg;
	llvm::Value* ar;
	llvm::Value* wr;

	llvm::BasicBlock* wrap = llvm::BasicBlock::Create(ctx, "wrap", current_func);
	llvm::BasicBlock* no_wrap = llvm::BasicBlock::Create(ctx, "no_wrap", current_func);
	llvm::BasicBlock* end = llvm::BasicBlock::Create(ctx, "end", current_func);

	if (current_val)
		ar = current_val;
	else
		ar = LoadRegister(ar_id, SEX_ZERO);
	ar = builder.CreateTrunc(ar, llvm::IntegerType::get(ctx, 32));

	wr = LoadRegister(wr_id, SEX_ZERO);
	wr = builder.CreateTrunc(wr, llvm::IntegerType::get(ctx, 32));

	llvm::Value* nar = builder.CreateAdd(ar, GenUInt32(1));

	llvm::Value* lhs = builder.CreateXor(ar, nar);
	llvm::Value* rhs = builder.CreateShl(builder.CreateOr(wr, GenUInt32(1)), GenUInt32(1));
	llvm::Value* cmp = builder.CreateICmpUGT(lhs, rhs);
	builder.CreateCondBr(cmp, wrap, no_wrap);

	builder.SetInsertPoint(wrap);

	llvm::Value* nar_wrapped = builder.CreateSub(nar, builder.CreateAdd(wr, GenUInt32(1)));
	builder.CreateBr(end);

	builder.SetInsertPoint(no_wrap);
	builder.CreateBr(end);

	builder.SetInsertPoint(end);
	llvm::PHINode* phi = builder.CreatePHI(llvm::IntegerType::get(ctx, 32), 2, "incremented_ar");
	phi->addIncoming(nar_wrapped, wrap);
	phi->addIncoming(nar, no_wrap);

	nar = builder.CreateTrunc(phi, llvm::IntegerType::get(ctx, 16));
	StoreRegister(ar_id, nar);
}
