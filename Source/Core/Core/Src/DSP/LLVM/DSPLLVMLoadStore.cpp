// Copyright (C) 2003 Dolphin Project.

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 2.0.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License 2.0 for more details.

// A copy of the GPL 2.0 should have been included with the program.
// If not, see http://www.gnu.org/licenses/

// Official SVN repository and contact information can be found at
// http://code.google.com/p/dolphin-emu/

#include "../DSPLLVMEmitter.h"

// SRRI @$D, $S
// 0001 1011 0dds ssss
// Store value from source register $S to a memory location pointed by
// addressing register $D. Increment register $D.
void DSPLLVMEmitter::srri(const UDSPInstruction opc)
{
	u8 dreg = (opc >> 5) & 0x3;
	u8 sreg = opc & 0x1f;

	llvm::Value* val = LoadRegister(sreg, SATURATE);
	llvm::Value* addr = LoadRegister(dreg, SEX_ZERO);

	WriteDMEM(addr, val);
	IncrementAddrReg(dreg, addr);
}
